require 'open-uri'

class Game
  include ActiveModel::Model

  attr_accessor :bgg_data

  def initialize(params)
    @bggId = params.fetch(:bggId, nil)
    
    raise ArgumentError, 'BGG ID is required' if @bggId.nil?
    raise ArgumentError, 'BGG ID needs to be an number' if @bggId.to_i == 0

    get_bgg_data(params.fetch(:bgg_data, false))
  end
  
  private
    def get_bgg_data(json)
      results = Nokogiri::XML(open("https://www.boardgamegeek.com/xmlapi/boardgame/#{@bggId}&stats=1"))

      if !results.xpath('//error')[0].nil?
        raise ArgumentError, results.xpath('//error')[0].attributes['message'].value
      end

      @bgg_data = Hash.from_xml(results.to_xml).to_json if json
      set_values(results) unless json
    end

    def set_values(data)
      @title = CGI.unescapeHTML(data.xpath("//name[@primary='true']").children.to_s)
      @minPlayers = data.xpath('//minplayers').children.to_s.to_i
      @maxPlayers = data.xpath('//maxplayers').children.to_s.to_i
      @minPlayingTime = data.xpath('//minplaytime').children.to_s.to_i
      @maxPlayingTime = data.xpath('//maxplaytime').children.to_s.to_i
      @categories = data.xpath('//boardgamecategory').children.map { |c| c.to_s }
      @mechanisms = data.xpath('//boardgamemechanic').children.map { |m| m.to_s }
      @difficulty = data.xpath('//averageweight').children.to_s.to_f.round(2)
      @thumbnail = data.xpath('//thumbnail').children.to_s
    end
end