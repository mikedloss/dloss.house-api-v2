class Api::GameController < ApplicationController

  def show
    bgg_data = params.key?('raw_data')
    game = Game.new(bggId: params['bggId'], bgg_data: bgg_data)

    render json: bgg_data ? game.bgg_data : game
  end
end
