Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    get '/game/:bggId', to: 'game#show', as: 'game'
    get '/game/:bggId/bgg', to: 'game#show', as: 'game_bgg', defaults: { raw_data: true }
  end
end
